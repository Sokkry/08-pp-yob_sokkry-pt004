
import React, { Component } from 'react'
import { Button, Card, Row, Col } from 'react-bootstrap'

export default class Mycard extends Component {
    constructor() {
        super()
        this.state = {
            card: [
                {
                    id: 1,
                    place: "Angkor 001",
                    country: "In Camboia",
                    images: "./images/angkor01.jpg"
                },
                {
                    id: 2,
                    place: "Angkor 002",
                    country: "In Cambodia",
                    images: "./images/angkor02.jpg"
                },
                {
                    id: 3,
                    place: "Angkor 003",
                    country: "In Cambodia",
                    images: "./images/angkor03.jpg"
                }
            ]
        }
    }
    render() {
        return (
            <div>
                <Row>
                    {this.state.card.map((item, index) => (
                        <Col>
                            <Card style={{ width: '18rem' }}>
                                <Card.Img variant="top" src={item.images} />
                                <Card.Body>
                                    <Card.Title>{item.place} </Card.Title>
                                    <Card.Text>{item.country}</Card.Text>
                                    <Button variant="primary">Delete</Button>
                                </Card.Body>
                            </Card>
                        </Col>
                    ))}
                </Row>

            </div>
        )
    }
}
