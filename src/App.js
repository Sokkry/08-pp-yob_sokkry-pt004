import logo from './logo.svg';
import './App.css';
import {Container, Row } from 'react-bootstrap'
import Mycard from './components/Mycard'
import Mymenu from './components/Mymenu'
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
    <Container>
      <Row>
        <Mymenu />
      </Row>
      <Row>
        <Mycard />
      </Row>
    </Container>
  );
}

export default App;
